# Video Gallery App


## TASK
Using a single JSON feed as the data source we would like you to build a page using Javascript,   
HTML and CSS to render the data. We would like to see a Javascript framework used if possible   
(React or vue.js preferably). We would like you to spend no more than 1-2 hours on this tasks   
and fully appreciate that it may not be completed within that time. Please ensure you structure   
you code in a clean way and comment where appropriate so that we can see where you were going!    


## IMPLEMENTATION

* To run app clone repo / download and unzip on local machine.

* Ensure Node JS amd npm are installed on local machine and in path.
  (https://docs.npmjs.com/getting-started/installing-node)

* Open terminal and navigate to directory where app files are located.

* Run command `npm install`

* This should load all dependencies into directory /node_modules

* Once this has completed run command `npm start`

* Open browser and navigate to `localhost: 8080`

* I hope you will be able to see my unfinished video gallery app.


## USEFUL INFORMATION

I included bootstrap and also my own custom scss.  
I wrote a gulp script to parse the scss and minify it.  
Should you need to update scss you will need to run dev script  
`gulp`  
in a different termial window from the one running react.  
When all changes are completed, exit script and close terminal window.  
(If you want to run all the time add `gulp` to the start command in package.json)


## TO DO

Although the brief has not been completed in full,   
hopefully you can see the direction the app is going.

The following are incomplete

* Complete the filters. these should add filtered values to search bar which can be removed singly or in one sweep.

* Popup video player

* Embed player for featured video so it plays in situ

* Extend routes declared in 'src/index.js' so that the channel id is not hard coded
i.e.  `<Route path="/channel/:id" component={ChannelShow} />`

* For production use app would need to include authentication check,  
  so we know who is watching and how much cpd they have achieved 

* Check if default Asset TV logo needs to replaced by customer logo for whitelabel / customer branding 

* Implement custom branding css from data source. Stubbed in 'video_channel_index.js'

* Process dates so that the machine date is converted into human dates i.e. dd mm yyy

* Paginate videos in each tab so that only 8, 12 or 16 show at one time

* Fine tune lazy load offset so that it loads at desired speed

* Review and refactor components where required, as additional functionality is added
