import axios from 'axios';

export const FETCH_VIDEOS = 'fetch_videos';

const ROOT_URL = 'https://titan.asset.tv/api/';
const VIDEO_API_KEY = '&key=api_key_here_if_required';

export function fetchVideos() {
  const request = axios.get(`${ROOT_URL}channel-view-json/2240`);
  // TODO - remove hard coded channel id number

  return {
    type: FETCH_VIDEOS,
    payload: request
  };
}
