import React , { Component } from 'react';

export default class SiteHeader extends Component { 
  render() {
    return (
      <header>    
        <nav className="navbar navbar-fixed-top">
          <div className="container">
            <div className="navbar-header">
              <button type="button" 
                      className="navbar-toggle collapsed" 
                      data-toggle="collapse" 
                      data-target="#navbar" 
                      aria-expanded="false" 
                      aria-controls="navbar"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="navbar-brand" href="#">
                <img src="assets/images/asset-tv-logo.jpg" />
              </a>
            </div>
            <div id="navbar" 
                 className="navbar-collapse collapse"
            >
              <ul className="nav navbar-nav">
                <li className="active">
                  <a href="#">Home</a>
                </li>
                <li>
                  <a href="#">Link 1</a></li>
                <li>
                  <a href="#">Link 2</a>
                </li>
                <li>
                  <a href="#">Link 3</a>
                </li>
              </ul>
              <div className="nav navbar-nav navbar-right">
                <ul className="nav navbar-nav navbar-tall">
                  <li className="nav-item-borders">
                    <a href="#">Login</a>
                  </li>
                  <li className="nav-item-borders">
                    <a href="#">Sign up</a>
                  </li>
                </ul>            
              </div>
            </div>
          </div>
        </nav>        
      </header>
    );
  }

}
