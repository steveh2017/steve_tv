import React , { Component } from 'react';

class VideoFilters extends Component {
  // TODO : connect filters up to use autocomplete array of keywords and
  // TODO : allow selection of terms and people from select lists
  // TODO : cancel all filters using link
   
  render() {
    return (
      <section className="video-filters">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
                <div className="video-filters-search">
                  <div className="input-group">
                    <input type="text"
                           className="form-control"
                           id="VideoFilterSearch"
                           placeholder="Keyword search"
                    />
                    <span className="input-group-btn">
                      <button className="btn btn-info" type="button">
                        <i className="glyphicon glyphicon-search"></i>
                      </button>
                    </span>
                  </div>
                </div>
            </div>

            <div className="col-md-3">
                <select className="form-control" 
                        id="VideoFilterPeople"
                >
                  <option value="label">Filter people</option>
                  <option value="1">1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </select>
            </div>

            <div className="col-md-3">
                <select className="form-control" 
                        id="VideoFilterTerms"
                >
                  <option value="label">Filter terms</option>
                  <option value="1">1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </select>
            </div>

            <div className="col-md-3">
                <select className="form-control"  
                        id="VideoFilterSort"
                >
                  <option value="label">Sort Results</option>
                  <option value="1">1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </select>
            </div>                
          </div>
          <div className="row">
            <div className="filter-keyword-label">
              Remove all keywords
            </div>
            <div id="DisplayFilterKeywords">
              <span className="filter-keyword filter-keyword-remove">
                Finance 
                <i className="glyphicon glyphicon-remove"></i>
              </span>
          </div>
          </div>
        </div>
      </section>
    );
  }

}

export default VideoFilters;  
