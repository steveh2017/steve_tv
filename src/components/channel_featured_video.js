import React , { Component } from 'react';
import renderHTML from 'react-render-html';


export default class ChannelFeaturedVideo extends Component {

  returnTidyString(str) {
    let newStr = _.replace(str, new RegExp('\r\n\r\n', 'g'), '<br />');
    return _.replace(newStr, new RegExp('\r\n', 'g'), ' ');
  }


  returnFeaturedVideo(tabString, galleryData){
    if(!this.props.mainChannelData){
      return '';
    }

    let tabIds = _.head(_.split(tabString, ','));
    return _.head(galleryData[tabIds]); 
  }


  render() {  
  
    if(!this.props.mainChannelData) {
      return (
        <div>Loading.....</div>
      );
    }

    let bgImage = {
      backgroundImage: `url('${this.props.mainChannelData.hdr_bg_1}')`
    }; 

    const mcd = this.props.mainChannelData;
    const tabData = this.props.tabData;
    const galleryData = this.props.videoGalleryData;
    const fv =  this.returnFeaturedVideo(mcd.tab_creator, galleryData);

    return (
      <main className="container">
        <div className="row">
          <div className="col-md-12 channel-header"
               style={bgImage} 
          >
            <img src={mcd.channel_header_logo} 
                 alt={mcd.title}
            />
          </div>
        </div>

        <div className="row channel-featured-video">
          <div className="col-md-6">
            <img src={fv.image_url}
                 alt={`${fv.title}, duration ${fv.duration}`}
            />
            <span className="channel-featured-video-duration">
              {fv.duration}
            </span>

            <span className="channel-featured-video-play">
              <i className="glyphicon glyphicon-triangle-right"></i>
            </span>        
          </div>
          <div className="col-md-6">
            <h3>
              {fv.title}
            </h3>

            <p className="channel-featured-video-desc">
              {renderHTML(this.returnTidyString(fv.description))}
            </p>
               
            <p className="channel-featured-video-date">
              Posted: {fv.date}
            </p>
          </div>
        </div>
      </main>
    );
  }

} 
