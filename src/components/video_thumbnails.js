import React , { Component } from 'react';
import LazyLoad from 'react-lazy-load';

class VideoThumbnails extends Component {

  renderVideoThumbnails(galleryData) {

    return _.map(galleryData, gal => {
      return (
        <div className="col-md-3">
          <div className="panel panel-default">
            <div className="panel-body">
              <div className="video-thumbnail">
                <a href="#!">
                  <LazyLoad  className="lazy-wrap" offsetTop={800}>
                    <img src={gal.image_url} 
                         alt={gal.title}
                    />
                  </LazyLoad>                  
                  <span className="video-thumbnail-play">
                    <i className="glyphicon glyphicon-triangle-right"></i>
                  </span>

                  <span className="video-thumbnail-duration">
                    {gal.duration}
                  </span>
                </a>                      
              </div>
              <div className="video-thumbnail-info">
                <h3 className="video-thumbnail-title">
                  <a href="#">{gal.title}</a>
                </h3>
                <p className="video-thumbnail-date">
                  Posted: {gal.date} 
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }



  render() {

    if(!this.props.galleryThumbnails) {
      return (
        'Loading....'
      )
    }


    return (
      <div className="row">
        {this.renderVideoThumbnails(this.props.galleryThumbnails)}
      </div>
    );
  }

}

export default VideoThumbnails;  
