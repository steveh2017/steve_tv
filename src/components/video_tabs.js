import React , { Component } from 'react';
import VideoThumbnails from './video_thumbnails';

class VideoTabs extends Component {

  returnArrayTabIds(tabString) {
    return _.split(tabString, ',');
  }

  returnLowercaseString(str) {
    let newStr = _.replace(str, new RegExp(' ', 'g'), '');
    return _.toLower(newStr);
  }

  returnFirstTabdId(tabArray) {
    return _.head(tabArray);
  }


  renderVideoTabHeads(tabString, tabData) {
    let tabArray = this.returnArrayTabIds(tabString)
    let firstTab = this.returnFirstTabdId(tabArray);
    return _.map(tabArray, tab => {
      let tabClass;
      if(firstTab === tab) {
        tabClass = 'active';
      }else{
        tabClass = '';
      }

      return (
        <li role="presentation" 
            className={tabClass}
            key={`tid${tab}`}
        >
          <a href={this.returnLowercaseString('#'+tabData[tab].tab_name)}
             aria-controls={this.returnLowercaseString(tabData[tab].tab_name)}
             role="tab" 
             data-toggle="tab"             
          >
            {tabData[tab].tab_name}
          </a>
        </li>
      );
    });
  }



  renderVideoTabContent(tabString, tabData, galleryData) {
    let tabArray = this.returnArrayTabIds(tabString);
    let firstTab = this.returnFirstTabdId(tabArray);
    return _.map(tabArray, tab => {
      let tabClass;
      if(firstTab === tab) {
        tabClass = 'tab-pane  active';
      }else{
        tabClass = 'tab-pane ';
      }

      return (
        <div role="tabpanel" 
             className={tabClass}
             id={this.returnLowercaseString(tabData[tab].tab_name)}
             key={`cid${tab}`}
        >
          <div className="container">
              <VideoThumbnails 
                galleryThumbnails={galleryData[tab]}
              />
          </div>
        </div>
      );
    });
  }


  render() {

    if(!this.props.mainChannelData) {
      return (
        <div>Loading.....</div>
      );
    }

    const mcd = this.props.mainChannelData;
    const tabData = this.props.tabData;
    const galleryData = this.props.videoGalleryData;

    return (
      <section className="video-tab-gallery">
        <div className="container">

          <ul className="nav nav-tabs" role="tablist">
            {this.renderVideoTabHeads(mcd.tab_creator, tabData)}

            <li role="presentation">
              <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                Other
              </a>
            </li>
          </ul>
          <div className="tab-content">
            {this.renderVideoTabContent(mcd.tab_creator, tabData, galleryData)}

            <div role="tabpanel" className="tab-pane" id="settings">
              <div className="container">
                <div className="row">

                <div className="col-md-6">
                  <p>Place any other useful info here.... <br /> 
                  e.g. regional specific cpd / graph of achievement <br />
                  For the avoidance of doubt, I would not call this tab 'other',<br />
                  I'm sure a more suitable name could be thought of.
                  </p>
                  <p>When videos have been watched consider flashing with 'Already watched'... as per example</p>
                </div>


                  <div className="col-md-3">
                    <div className="panel panel-default">
                      <div className="panel-body video-thumbnail-watched">
                        <div className="video-thumbnail">
                          <a href="#!">
                            <img src="/assets/images/1502183476526_Titan-and-MM-Video-Grabs.jpg" />
                            <span className="video-thumbnail-play">
                              <i className="glyphicon glyphicon-triangle-right"></i>
                            </span>

                            <span className="video-thumbnail-duration">
                              40:47
                            </span>
                          </a>                      
                        </div>
                        <h3 className="video-thumbnail-title">
                          <a href="#">ESG | Masterclass</a>
                        </h3>
                        <p className="video-thumbnail-date">
                          Posted: 2017/07/27 
                        </p>
                        <div className="video-thumbnail-watched-ribbon">
                          Already watched
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              
            </div>
          </div>
        </div>
      </section>
    );
  }

}

export default VideoTabs;  
