import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchVideos } from '../actions';

import SiteHeader from './site_header';
import ChannelFeaturedVideo from './channel_featured_video';
import VideoFilters from './video_filters';
import VideoTabs from './video_tabs';
import SiteFooter from './site_footer';


class VideoChannelIndex extends Component {

  componentDidMount() {
    this.props.fetchVideos();
  }



  returnCustomCss() {
    // Do not know how you want to handle this.
    // I am guessing you change BG colour of page,
    // or use it for cutomer specific branding.
    return this.props.videos.mcd.custom_css;
  }


  render() {  

    if(!this.props.videos) {
      return (
        <div>Loading.....</div>
      );
    }
 
    return(
      <div className="full-width">
        <SiteHeader />
        <ChannelFeaturedVideo 
          mainChannelData={this.props.videos.mcd}
          tabData={this.props.videos.tabs}
          videoGalleryData={this.props.videos.content}
        />
        <VideoFilters />
        <VideoTabs 
          mainChannelData={this.props.videos.mcd}
          tabData={this.props.videos.tabs}
          videoGalleryData={this.props.videos.content}
        />
        <SiteFooter />
      </div>
    );
  }
}


function mapStateToProps(state){
  return { videos: state.videos };
}

export default connect(mapStateToProps, {fetchVideos: fetchVideos})(VideoChannelIndex);
