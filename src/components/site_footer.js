import React , { Component } from 'react';

export default class SiteFooter extends Component {

  render() {
    return (
      <footer>
        <div className="container">
          <p>&copy; Video gallery created for Asset TV by Steve Hawkes</p>
        </div>
      </footer>
    );
  }

}
 
